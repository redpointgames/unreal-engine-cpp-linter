﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ClangSharp;
using ClangSharp.Interop;
using Microsoft.Build.Locator;
using net.r_eg.MvsSln;
using net.r_eg.MvsSln.Core;

namespace clangtest2
{
    class Program
    {
        static void Main(string[] args)
        {
            MSBuildLocator.RegisterDefaults();

            LoadSln(args[0]);
        }

        public class MyEnv : IsolatedEnv, IEnvironment, IDisposable
        {
            public MyEnv(ISlnResult data)
                : base(data)
            {

            }

            protected override Microsoft.Build.Evaluation.Project Load(string path, IDictionary<string, string> properties)
            {
                // TODO: Fix this to use "vswhere" to locate the VC++ files.
                properties.Add("VCTargetsPath", @"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Microsoft\VC\v160\");
                return base.Load(path, properties);
            }

            protected override void Dispose(bool disposing) => base.Dispose(disposing);
        }

        static string ResolvePath(string projectPath, string candidatePath)
        {
            if (Path.IsPathRooted(candidatePath))
            {
                return candidatePath;
            }

            var absolutePath = Path.Combine(projectPath, candidatePath);
            if (Directory.Exists(absolutePath) || File.Exists(absolutePath))
            {
                return Path.GetFullPath(absolutePath);
            }

            return null;
        }

        static void LoadSln(string slnPath)
        {
            using (var sln = new Sln(slnPath, SlnItems.Projects | SlnItems.SolutionConfPlatforms
                                | SlnItems.ProjectConfPlatforms))
            {
                IEnvironment env = new MyEnv(sln.Result);
                env.LoadMinimalProjects();

                var project = env.GetOrLoadProject(
                    sln.Result.ProjectItems.FirstOrDefault(x => x.name != "UE4"));

                // Compute the common include paths.
                var includePathsRaw = project.GetPropertyValue("IncludePath").Split(";");
                var includePathsResolved = includePathsRaw.Select(x => ResolvePath(project.DirectoryPath, x)).Where(x => x != null).ToList();
                var includePaths = includePathsResolved.Select(x => "--include-directory=" + x).ToArray();

                // For each ClCompile entry, go and perform the AST linting.
                CXIndex index = CXIndex.Create();

                var tasks = new List<Task>();
                var clCompiles = project.GetItems("ClCompile").Where(x => !(x.EvaluatedInclude.Contains("XboxOne") ||
                        x.EvaluatedInclude.Contains("PS4") ||
                        x.EvaluatedInclude.Contains("Switch"))).ToList();
                fileIndexGlobal = 1;
                foreach (var compile in clCompiles)
                {
                    tasks.Add(Task.Run(async () =>
                    {
                        try
                        {
                            await ClangMain(clCompiles.Count, index, ResolvePath(project.DirectoryPath, compile.EvaluatedInclude), compile, includePaths);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"exception: {ex.ToString()}");
                        }
                    }));
                }
                Task.WaitAll(tasks.ToArray());

                return;
            }
        }

        static SemaphoreSlim fileIndexSem = new SemaphoreSlim(1);
        static int fileIndexGlobal = 1;

        static async Task ClangMain(int fileTotal, CXIndex index, string filePath, Microsoft.Build.Evaluation.ProjectItem clCompile, string[] includePaths)
        {
            if (filePath == null)
            {
                return;
            }

            var additionalIncludePaths = clCompile.GetMetadataValue("AdditionalIncludeDirectories").Split(";").Where(x => Directory.Exists(x)).Select(x => "--include-directory=" + x);
            var forcedIncludeFiles = clCompile.GetMetadataValue("ForcedIncludeFiles").Split(";").Where(x => File.Exists(x)).Select(x => "--include=" + x);

            var commandArgs = includePaths.Concat(additionalIncludePaths).Concat(forcedIncludeFiles).Concat(new string[] {
                "-fdiagnostics-format=msvc",
                "-Wno-unused-private-field",
                "-Wno-tautological-compare",
                "-Wno-undefined-bool-conversion",
                "-Wno-unused-local-typedef",
                "-Wno-inconsistent-missing-override",
                "-Wno-undefined-var-template",
                "-Wno-unused-lambda-capture",
                "-Wno-unused-variable",
                "-Wno-unused-function",
                "-Wno-switch",
                "-Wno-unknown-pragmas",
                "-Wno-invalid-offsetof",
                "-Wno-gnu-string-literal-operator-template",
                "-Wno-nonportable-include-path",
                "-Wno-ignored-attributes",
                "-Wno-microsoft-unqualified-friend",
                "-Wno-implicit-exception-spec-mismatch",
                "-std=c++17",
                "-DUNREAL_CODE_ANALYZER=1",
            }).ToArray();

            await fileIndexSem.WaitAsync();
            int fileIndex = fileIndexGlobal++;
            Console.WriteLine($"[{fileIndex}/{fileTotal}] {filePath}");
            fileIndexSem.Release();

            var translationUnitError = CXTranslationUnit.TryParse(index,
                filePath,
                commandArgs,
                Array.Empty<CXUnsavedFile>(),
                CXTranslationUnit_Flags.CXTranslationUnit_IgnoreNonErrorsFromIncludedFiles,
                out CXTranslationUnit handle);
            var skipProcessing = false;

            if (translationUnitError != CXErrorCode.CXError_Success)
            {
                Console.WriteLine($"[{fileIndex}/{fileTotal}] fatal error while processing");
                skipProcessing = true;
            }
            else if (handle.NumDiagnostics != 0)
            {
                var errorCount = 0;
                for (uint i = 0; i < handle.NumDiagnostics; ++i)
                {
                    using var diagnostic = handle.GetDiagnostic(i);

                    if (diagnostic.Severity == CXDiagnosticSeverity.CXDiagnostic_Error || diagnostic.Severity == CXDiagnosticSeverity.CXDiagnostic_Fatal)
                    {
                        errorCount++;
                    }

                    Console.WriteLine($"[{fileIndex}/{fileTotal}] error: " + diagnostic.Format(CXDiagnostic.DefaultDisplayOptions).ToString());
                }
                if (errorCount > 0)
                {
                    skipProcessing = true;
                }
            }

            if (skipProcessing)
            {
                return;
            }

            using (var translationUnit = TranslationUnit.GetOrCreate(handle))
            {
                if (translationUnit == null)
                {
                    Console.WriteLine($"[{fileIndex}/{fileTotal}] error: no translation unit");
                    return;
                }

                Stack<Cursor> stackCursor = new Stack<Cursor>();
                Stack<int> stackChildIndex = new Stack<int>();
                stackCursor.Push(translationUnit.TranslationUnitDecl);
                stackChildIndex.Push(0);
                var childIndex = 0;
                while (stackCursor.Count > 0)
                {
                    var parent = stackCursor.Peek();
                    var parentChildCount = parent.CursorChildren.Count;

                    // Check if we have run out of children.
                    if (childIndex >= parentChildCount)
                    {
                        stackCursor.Pop();
                        childIndex = stackChildIndex.Pop() + 1;
                        continue;
                    }

                    // Get the current element.
                    var current = parent.CursorChildren[childIndex];

                    // Ignore these elements entirely.
                    if (!current.Extent.Start.IsFromMainFile)
                    {
                        childIndex++;
                        continue;
                    }

                    current.Location.GetFileLocation(out CXFile file, out uint line, out uint column, out uint offset);

                    // These are the linter checks - currently this is specific to EOS but you can adapt it to your own purposes.
                    if (current is VarDecl varDecl)
                    {
                        if (varDecl.CursorParent is DeclStmt)
                        {
                            if (varDecl.Type.ToString().StartsWith("EOS_"))
                            {
                                if (!varDecl.HasInit)
                                {
                                    if (varDecl.Type.TypeClass == CX_TypeClass.CX_TypeClass_Pointer)
                                    {
                                        Console.WriteLine($"{file.Name}({line},{column}): warning EOS001: Missing initializer for an EOS pointer. Use '{varDecl.Type.ToString()} {varDecl.Spelling} = nullptr' to initialize it to the zero value correctly.");
                                    }
                                    else
                                    {
                                        Console.WriteLine($"{file.Name}({line},{column}): warning EOS001: Missing initializer for an EOS structure. Use '{varDecl.Type.ToString()} {varDecl.Spelling} = {{}}' to initialize it to the zero value correctly.");
                                    }
                                }
                            }
                        }
                    }

                    // If we have children, push the current element onto
                    // the stack for later, and then start processing children.
                    if (current.CursorChildren.Count > 0)
                    {
                        stackCursor.Push(current);
                        stackChildIndex.Push(childIndex);
                        childIndex = 0;
                    }
                    else
                    {
                        childIndex++;
                    }
                }
            }
        }
    }
}
